package me.blha303.shadowban;

import com.evilmidget38.UUIDFetcher;
import com.google.common.base.Joiner;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;


public class ShadowBan extends JavaPlugin implements Listener
{

    Set<UUID> playerSet = Collections.synchronizedSet( new HashSet<UUID>() );
    String prefix = ChatColor.GRAY + "[" + ChatColor.AQUA + getDescription().getName() + ChatColor.GRAY + "] ";
    boolean debug = false;

    public void onEnable()
    {
        saveDefaultConfig();
        debug = getConfig().getBoolean( "debug" );
        getServer().getPluginManager().registerEvents( this, this );
        debug( "Events registered" );
        loadBannedUsers();
    }

    private void loadBannedUsers()
    {
        playerSet.clear();
        for ( String uuidString : getConfig().getStringList( "shadowbannedList" ) )
        {
            UUID uuid = UUID.fromString( uuidString );
            playerSet.add( uuid );
        }
    }

    private void saveBannedUsers()
    {
        List<String> list = new ArrayList<String>();
        for ( UUID uuid : playerSet )
        {
            list.add( uuid.toString() );
        }
        getConfig().set( "shadowbannedList", list );
    }

    public void onDisable()
    {
        saveConfig();
    }

    public void debug( String msg )
    {
        if ( debug )
        {
            this.getLogger().info( "DEBUG: " + msg );
        }
    }

    @EventHandler( priority = EventPriority.LOWEST )
    public void onPlayerChat( AsyncPlayerChatEvent event )
    {
        debug( "Entered PlayerChatEvent" );
        if ( playerSet.contains( event.getPlayer().getUniqueId() ) )
        {
            debug( "Playerlist contains this name!" );
            event.getRecipients().clear();
            debug( "List cleared" );
            event.getRecipients().add( event.getPlayer() );
            debug( "Recipient added" );
            debug( "Message sent" );
        }
    }

    public boolean onCommand( CommandSender sender, Command command, String label, String[] args )
    {
        if ( command.getLabel().equalsIgnoreCase( "shadowban" ) )
        {
            shadowBanCommand( sender, command, label, args );
            return true;
        }
        else if ( command.getLabel().equalsIgnoreCase( "shadowbanoffline" ) )
        {
            shadowBanOfflineCommand( sender, command, label, args );
            return true;
        }
        else if ( command.getLabel().equalsIgnoreCase( "shadowunban" ) )
        {
            shadowUnbanCommand( sender, command, label, args );
            return true;
        }
        else if ( command.getLabel().equalsIgnoreCase( "shadowbanreload" ) )
        {
            if ( sender.hasPermission( "shadowban.reload" ) )
            {
                loadBannedUsers();
                sender.sendMessage( prefix + ChatColor.WHITE + "Config reloaded." );
                return true;
            }
            else
            {
                sender.sendMessage( prefix + "You can't use this command." );
                return true;
            }
        }
        else if ( command.getLabel().equalsIgnoreCase( "shadowbanlist" ) )
        {
            if ( sender.hasPermission( "shadowban.list" ) )
            {
                String list = ChatColor.GREEN + Joiner.on( ChatColor.WHITE + ", " + ChatColor.GREEN ).join( playerSet );
                sender.sendMessage( prefix + list );
                return true;
            }
            else
            {
                sender.sendMessage( prefix + "You can't use this command." );
                return true;
            }
        }
        return false;
    }

    private boolean shadowUnbanCommand( CommandSender sender, Command command, String label, String[] args )
    {
        if ( args.length >= 1 )
        {
            if ( sender instanceof ConsoleCommandSender || sender.hasPermission( "shadowban.unban" ) )
            {
                debug( "One or more args" );
                for ( String arg : args )
                {
                    debug( "Entered for loop" );
                    debug( "Removing arg from playerlist" );
                    Player player = Bukkit.getPlayer( arg );
                    if ( player == null )
                    {
                        shadowUnbanOffline( arg, sender );
                    }
                    else
                    {
                        debug( "UUID: " + player.getUniqueId() );
                        playerSet.remove( player.getUniqueId() );
                        debug( "arg removed" );
                        saveBannedUsers();
                        saveConfig();
                        sender.sendMessage( prefix + ChatColor.GREEN + arg + " " + ChatColor.WHITE + "removed from shadowban list" );
                        return true;
                    }

                }
            }
            else
            {
                sender.sendMessage( prefix + "You can't use this command." );
                return true;
            }
        }
        else
        {
            debug( "not enough args" );
            return false;
        }
        return false;
    }

    private boolean shadowBanOfflineCommand( final CommandSender sender, Command command, String label, String[] args )
    {
        if ( args.length >= 1 )
        {
            if ( sender instanceof ConsoleCommandSender || sender.hasPermission( "shadowban.offline" ) )
            {
                debug( "One or more args" );
                for ( String arg : args )
                {
                    debug( "Entered for loop" );
                    shadowBanOffline( arg, sender );
                    return true;
                }
            }
            else
            {
                sender.sendMessage( prefix + "You can't use this command." );
                return true;
            }
        }
        else
        {
            debug( "not enough args" );
            return false;
        }
        return false;
    }

    private void shadowBanOffline( final String username, final CommandSender sender )
    {
        offlineBanOrUnban( username, sender, false );
    }

    private void shadowUnbanOffline( final String username, final CommandSender sender )
    {
        offlineBanOrUnban( username, sender, true );
    }

    private void offlineBanOrUnban( final String username, final CommandSender sender, final boolean unban )
    {
        new BukkitRunnable()
        {
            public void run()
            {
                UUIDFetcher fetcher = new UUIDFetcher( Collections.singletonList( username ) );
                try
                {
                    Map<String, UUID> response = fetcher.call();
                    for ( Map.Entry<String, UUID> entry : response.entrySet() )
                    {
                        if ( unban )
                        {
                            debug( "UUID: " + entry.getValue() );
                            playerSet.remove( entry.getValue() );
                        }
                        else
                        {
                            playerSet.add( entry.getValue() );
                        }
                        break;
                    }
                    debug( "arg added" );
                    saveBannedUsers();
                    saveConfig();
                    if ( unban )
                    {
                        sender.sendMessage( prefix + ChatColor.GREEN + username + " " + ChatColor.WHITE + "removed from shadowban list" );
                    }
                    else
                    {
                        sender.sendMessage( prefix + ChatColor.GREEN + username + " " + ChatColor.WHITE + "added to shadowban list" );
                    }
                }
                catch ( Exception e )
                {
                    e.printStackTrace();
                }

            }
        }.runTaskAsynchronously( this );

    }

    private boolean shadowBanCommand( CommandSender sender, Command command, String label, String[] args )
    {
        if ( args.length >= 1 )
        {
            if ( sender instanceof ConsoleCommandSender || sender.hasPermission( "shadowban.use" ) )
            {
                debug( "One or more args" );
                for ( String arg : args )
                {
                    debug( "Entered for loop" );
                    if ( getServer().getPlayer( arg ) != null )
                    {
                        debug( "Adding arg to playerlist" );
                        playerSet.add( getServer().getPlayer( arg ).getUniqueId() );
                        debug( "arg added" );
                    }
                    else
                    {
                        debug( "couldn't find player from arg" );
                        sender.sendMessage( prefix + ChatColor.WHITE + "Could not find player \"" + ChatColor.RED + arg + ChatColor.WHITE + "\"" );
                        return true;
                    }
                    saveBannedUsers();
                    saveConfig();
                    sender.sendMessage( prefix + ChatColor.GREEN + getServer().getPlayer( arg ).getName() + " " + ChatColor.WHITE + "added to shadowban list" );
                    return true;
                }
            }
            else
            {
                sender.sendMessage( prefix + "You can't use this command." );
                return true;
            }
        }
        else
        {
            debug( "not enough args" );
            return false;
        }
        return false;
    }
}
