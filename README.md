# ShadowBan
This plugin attempts to duplicate reddit's shadow-ban feature. Once shadow-banned, any messages from the player are hidden to everyone but themselves.

## Old More Obvious Functionality
This gives you the gist of what this plugin does.  Our iteration of this plugin makes you appear more normal in chat including your Prefix's and Suffix's etc.

![Banned player view](http://i.imgur.com/qS1JF0R.png) *Banned player*

![View for everyone else](http://i.imgur.com/EnJ2GVk.png) *Everyone else*

## Commands
#### Main Commands
|command|description|permission node|
|:--|:--:|:--|
|/shadowban [player]|Shadowbans a player (hides their chat messages from everyone else) - Alias: **/sban**|shadowban.use|
|/shadowunban [player]|Removes the shadowban from a player (requires their exact username) - Alias: **/sunban** |shadowban.unban|
|/shadowbanlist|Lists all current shadowbans - Alias: **/sbanl**|shadowban.list|

#### Reload Config
|command|description|permission node|
|:--|:--:|:--|
|/shadowbanreload|Reloads ShadowBan's config from file, discarding changes - Alias: **/sbanr** | shadowban.reload|

#### To be deprecated
|command|description|permission node|
|:--|:--:|:--|
|/shadowbanoffline [player]|Shadowbans a player without checking for valid username - Alias: **/sbano**|shadowban.offline|


Links
--------
Development builds of this project can be acquired at the provided continuous integration server. These builds have not been approved by the BukkitDev staff. Use them at your own risk.

* Jenkins: [ci.athion.net/job/ShadowBan](http://ci.athion.net/view/blha303/job/ShadowBan/) [Currently Not Available Publicly]
